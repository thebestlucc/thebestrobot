package TheBestRobot;
import robocode.*;
import robocode.util.Utils;
import java.awt.Color;
import java.awt.geom.*;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * EuRobo - a robot by thebestlucc
 */
public class TheBestRobot extends AdvancedRobot
{
	private byte moveDirection = 1;
	public void run() {
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		int counter = 0;
		while(true) {
			if(getRadarTurnRemaining() == 0.0)
				setTurnRadarRightRadians(Double.POSITIVE_INFINITY);
			execute();
		}
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {
		frita();
		double absoluteBearing = e.getBearingRadians() + getHeadingRadians();// Posição absoluta do inimigo
		double amountToTurn = Utils.normalRelativeAngle(Utils.normalRelativeAngle(absoluteBearing - getRadarHeadingRadians())); // quantidade para virar o radar baseado na posição absoluta do inimigo
		double amountExtraToTurn = Math.atan(36.0 / e.getDistance() * amountToTurn >= 0? 1 : -1);

		// rotaciona o radar com um range extra para evitar de perder o alvo de vista
		setTurnRadarRightRadians(amountToTurn + amountExtraToTurn);
		double distanceToEnemy = (e.getDistance()-300)/5 * -Math.signum(getVelocity());
		
		// movimentação lateral
		setTurnRight(e.getBearing() + 90 + distanceToEnemy);
		// anda
		setAhead(100 * moveDirection);
		// strafe by changing direction every 20 ticks
		if (getTime() % 20 == 0) {
			moveDirection *= -1;
			setAhead(250 * moveDirection);
		}
		
		// muda a direção caso o robo pare
		if (getVelocity() == 0)
			moveDirection *= -1;
		showTime(e); // função de atirar
		
		// Wall Smoothing
		double goalDirection = absoluteBearing - Math.PI/2 *moveDirection;
		Rectangle2D fieldRect = new Rectangle2D.Double(18, 18, getBattleFieldWidth()-36, getBattleFieldHeight()-36);
		while (!fieldRect.contains(getX()+Math.sin(goalDirection)*120, getY()+ Math.cos(goalDirection)*120))
		{
			goalDirection += moveDirection*.1;	//turn a little toward enemy and try again
		}
		double turn = robocode.util.Utils.normalRelativeAngle(goalDirection-getHeadingRadians());
		if (Math.abs(turn) > Math.PI/2)
		{
			turn = robocode.util.Utils.normalRelativeAngle(turn + Math.PI);
			setBack(100);
		}
		else
			setAhead(100);
		setTurnRightRadians(turn);
	}
	// atira uns feijãozinho
	public void showTime(ScannedRobotEvent e) {
		double firePower = decideFirePower(e);
		double absoluteBearing = e.getBearingRadians() + getHeadingRadians();// Posição absoluta do inimigo
		double amountToTurn = absoluteBearing - getGunHeadingRadians();
		double futurePoint = e.getVelocity() * Math.sin(e.getHeadingRadians() - absoluteBearing) / Rules.getBulletSpeed(1);

		setTurnGunRightRadians(Utils.normalRelativeAngle(amountToTurn + futurePoint));
		//Evitar tiro prematuro
		if (getGunHeat() == 0 && Math.abs(getGunTurnRemaining()) < 10)
			setFire(firePower);

		if(Math.random()>.8)
			setMaxVelocity((12*Math.random())+13);//muda velocidade de forma aleatoria
	}
	
	public double decideFirePower(ScannedRobotEvent e) {
		double firePower = getOthers() == 1 ? 2.0 : 3.0;
		
		// controla a força do tiro segundo a distancia
		if (e.getDistance() > 400) {
			firePower = 1.0;
		} else if( e.getDistance() < 200) {
			firePower = 3.0;
		}
		
		// controla força do tiro conforme a energya atual do robo
		if (getEnergy() < 1) {
			firePower = 0.1;
		}else if (getEnergy() < 10) {
			firePower = 1.0;
		}
		return Math.min(e.getEnergy() / 4, firePower);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		moveDirection *= -1;
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		moveDirection *= -1;
	}
	public void onHitRobot(HitRobotEvent e) {
		setFire(3);
	}
	//psicodelia das cores
	public void frita(){
		setColors(new Color((float)Math.random(),(float)Math.random(),(float)Math.random()),new Color((float)Math.random(),(float)Math.random(),(float)Math.random()),new Color((float)Math.random(),(float)Math.random(),(float)Math.random()));
	}
}
