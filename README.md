# TheBestRobot
  

_ThebestRobot, o robô do ThebestLucc. rs_

  

### Descrição

  

O TheBestRobot é um robô que utiliza da movimentação lateral perpendicular a posição do alvo que esteja mirando e se mantem em constante movimento para evitar ao máximo ser atingido por _bullets_ inimigas.

  

**AH, ELE CURTE UMAS FRITAÇÃOZINHA** ~~igual o pseudo-pai, vulgo _TheBestLucc_~~

Para simbolizar seu gosto por música, _sua cor é alterada de forma aleatória através da função **frita()** invocada a cada robô inimigo detectado pelo radar_

  

## Wall Smoothing

Foi utilizado o _Simple Iterative Wall Smoothing_ para evitar colisões com as paredes. O método consiste em limitar a área de movimentação do robô criando um _BattleField Virtual_ com área menor do que o campo de batalha real.

  

## Targetting and Shooting

_Apelidei carinhosamente o método de **Método Every breath you take**_ ~~I'll be watchin' you~~

O robô utiliza de uma _Mira Linear_ para se monitorar um alvo e calcular uma posição futura de tiro.

  

#### Pontos positivos

- Graças à este método, dificilmente um robô será perdido de vista.

- Robôs que mantem uma movimentação constante são alvos fáceis _Every Breath

#### Pontos negativos

- Robôs que alteram sua velocidade e direção de movimento de forma aleatória são mais difíceis de acertar

- Foi identificado durante as batalhas de teste que demora até que o alvo seja alterado depois de um alvo ser escolhido 

  

#O que eu mais gostei de ter feito/aprendido

De modo geral, o que mais gostei de ter feito/aprendido, sequer foi usado nesse robô, que foi a implementação de uma [Quadtree](https://en.wikipedia.org/wiki/Quadtree) e a experiência de adaptar o código da QuadTree para a linguagem Java, já que os vídeos explicativos estavam sendo feitos em JavaScript. ~~Isso acabou ficando de lado devido ao tempo de entrega estar se aproximando do fim, acabou chegando num nível de complexidade que talvez não conseguisse entregar nada se continuasse por esse caminho~~

Mas o que mais me impactou, com toda a certeza do mundo foi eu ter voltado a enxergar o quanto gosto de mexer com programação, já que há algum tempo eu estava desanimado e com bastante dúvidas sobre se era o que eu realmente queria fazer. ~~Efeitos da quarentena~~ 

###### Créditos
_Simple Iterative Wall Smoothing:_ [PEZ](https://robowiki.net/wiki/User:PEZ)